#!/usr/bin/env sh
# SPDX-License-Identifier: Unlicense
# SPDX-FileCopyrightText: 2023 Vieno Hakkerinen <vieno@hakkerinen.eu>

build() {
	for i in bullseye bookworm sid; do
		buildah bud\
			--build-arg username="$(head /dev/urandom | tr -dc a-z |\
				head -c 5)" -f debian_${i}.oci\
			-t registry.gitlab.com/txt.file/oci-container/debian:${i}
	done
	buildah bud --build-arg username="$(head /dev/urandom | tr -dc a-z |\
		head -c 5)" -f gentoo.oci\
		-t registry.gitlab.com/txt.file/oci-container/gentoo:latest
}

login() {
	buildah login registry.gitlab.com
}

upload() {
	for i in bullseye bookworm sid; do
		buildah push registry.gitlab.com/txt.file/oci-container/debian:${i}
	done
	buildah push registry.gitlab.com/txt.file/oci-container/gentoo:latest
}

if [ $# -ne 1 ]; then
	echo "I need one argument. I got $#."
	return 1
fi

case $1 in
	build)
		build
		return
		;;
	login)
		login
		return
		;;
	upload)
		upload
		return
		;;
	*)
		echo "Argument unknown"
		return 1
		;;
esac
