<!-- SPDX-License-Identifier: Unlicense -->
<!-- SPDX-FileCopyrightText: 2023 Vieno Hakkerinen <vieno@hakkerinen.eu> -->
# OCI containers

This is a collection of Dockerfile compatible files to build OCI compatible
containers.

Use `./helper.sh` with `build`, `login` or `upload` as argument. To run stuff
manually take a look at the commands in `helper.sh`.

## license

All files have a proper (REUSE)[https://reuse.software/] license & copyright
header.
